# RubberDuckyDojo

Le but de ce dojo est de découvrir le [Rubber Ducky](), une clé USB permettant de faire des attaques sur l'ordinateur qui la connecte.

Les liens de départ sont :
- https://docs.hak5.org/hc/en-us/articles/360010555153-Ducky-Script-the-USB-Rubber-Ducky-language
- https://docs.hak5.org/hc/en-us/articles/360010471234-Writing-your-first-USB-Rubber-Ducky-Payload

Nous avons choisi de faire un [script](./rubber.txt) pour le sytème d'exploitation Max Os X.

## Compilation

Le site https://ducktoolkit.com/encode permet de compiler le code Rubber Ducky (`Inject.bin`) afin d'avoir un exécutable à placer sur la clé.
Il faut sélectionner le "language", ce qui signifie la disposition de clavier de l'ordinateur ciblé.

